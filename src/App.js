const os = require("os"),
    fs = require("fs"),
    chokidar = require("chokidar"),
    isArray = require("@nathanfaucett/is_array"),
    isFunction = require("@nathanfaucett/is_function"),
    isNullOrUndefined = require("@nathanfaucett/is_null_or_undefined"),
    emptyFunction = require("@nathanfaucett/empty_function"),
    asyncDone = require("@nathanfaucett/async_done"),
    extend = require("@nathanfaucett/extend"),
    path = require("@nathanfaucett/file_path"),
    objectForEach = require("@nathanfaucett/object-for_each"),
    fileUtils = require("@nathanfaucett/file_utils");

class App {
    constructor(appRootPath) {
        this.rootPath = path.dir(__dirname);

        this.appRootPath = appRootPath;
        this.appConfigPath = path.join(this.appRootPath, "config");

        this.env = this.getEnv();

        this.config = this.getConfig();
        this.paths = this.getPaths();

        this.getTasks();

        this.data = {};
    }

    useGulp(gulp) {
        this.gulp = gulp;

        objectForEach(this.tasks, (task, name) => {
            gulp.task(name, task);
        });
    }

    refresh() {
        this.env = this.getEnv();

        this.config = this.getConfig();
        this.paths = this.getPaths();

        this.getTasks();
    }

    getEnvVar(key, defaults) {
        return process.env[key] || defaults;
    }
    getEnv() {
        return this.getEnvVar("NODE_ENV", "development");
    }

    requireAppModule(relativePath) {
        const fullPath = path.join(this.appRootPath, relativePath) + ".js";

        if (fs.existsSync(fullPath)) {
            delete require.cache[fullPath];
            return require(fullPath);
        } else {
            return null;
        }
    }

    requireModule(relativePath) {
        const fullPath = require.resolve(relativePath);
        delete require.cache[fullPath];
        return require(fullPath);
    }

    getDefaultPaths() {
        const appRootPath = this.appRootPath,
            appConfigPath = this.appConfigPath,
            appSrc = path.join(appRootPath, "app"),
            appDest = path.join(appRootPath, "build"),
            configSrc = path.join(appRootPath, "config"),
            cacheDir = path.join(appRootPath, ".cache"),
            jsSrc = path.join(appSrc, "js"),
            cssSrc = path.join(appSrc, "css");

        return {
            appSrc: appSrc,
            appDest: appDest,

            configSrc: configSrc,

            jsSrc: jsSrc,
            cssSrc: cssSrc,

            cacheDir: cacheDir,

            localeSrc: path.join(appConfigPath, "locale"),
            localeDest: path.join(appDest, "locale"),

            fontsSrc: path.join(appSrc, "fonts"),
            fontsDest: path.join(appDest, "fonts"),

            imgSrc: path.join(appSrc, "img"),
            imgDest: path.join(appDest, "img"),

            htmlIndexSrc: path.join(appSrc, "index.html"),
            htmlIndexDest: path.join(appDest, "index.html"),

            ejsIndexSrc: path.join(appSrc, "index.ejs"),
            ejsIndexDest: path.join(appDest, "index.html"),

            jsIndexSrc: path.join(jsSrc, "index.js"),
            jsIndexDest: path.join(appDest, "*.js"),
            jsIndexMapDest: path.join(appDest, "*.js.map"),

            cssIndexSrc: path.join(cssSrc, "index.less"),
            cssIndexDest: path.join(appDest, "index.css")
        };
    }
    getPaths() {
        let defaults = this.getDefaultPaths();

        const appDefaultPaths = this.requireAppModule("config/paths/defaults"),
            appPaths = this.requireAppModule("config/paths/" + this.getEnv());

        if (appDefaultPaths) {
            defaults = extend(defaults, appDefaultPaths(this, defaults));
        }

        if (appPaths) {
            return extend(defaults, appPaths(this, defaults));
        } else {
            return defaults;
        }
    }

    getDefaultConfig() {
        return {
            flatLocaleMode: true,
            locales: ["en"],
            throwMissingTranslationError: false,
            html5Mode: false,
            liveReloadPort: 35729,
            appUrl: "http://localhost:" + this.getEnvVar("PORT", 8080)
        };
    }
    getConfig() {
        let defaults = this.getDefaultConfig();

        const appDefaultConfig = this.requireAppModule(
                "config/config/defaults"
            ),
            appConfig = this.requireAppModule("config/config/" + this.getEnv());

        if (appDefaultConfig) {
            defaults = extend(defaults, appDefaultConfig(this, defaults));
        }

        if (appConfig) {
            return extend(defaults, appConfig(this, defaults));
        } else {
            return defaults;
        }

        return extend(defaults, overrides);
    }

    getTaskClass(name) {
        return this.taskClasses[name];
    }
    getTask(name) {
        return this.tasks[name];
    }
    runTask(name, callback) {
        return asyncDone(this.getTask(name), callback);
    }
    watch(glob, options, callback) {
        if (isFunction(options)) {
            callback = options;
            options = {};
        }
        if (!isFunction(callback) || isArray(callback)) {
            throw new TypeError(
                "watch " + glob + " watch callback has to be a function"
            );
        }

        options = options || {};

        if (isNullOrUndefined(options.ignoreInitial)) {
            options.ignoreInitial = true;
        }

        let watcher = chokidar.watch(glob, options);

        if (callback) {
            let fn = () => {
                callback(emptyFunction);
            };

            watcher
                .on("change", fn)
                .on("unlink", fn)
                .on("add", fn);
        }

        return watcher;
    }

    requreTasks(taskPath) {
        const taskClasses = {};

        if (fs.existsSync(taskPath)) {
            fileUtils.diveSync(taskPath, stat => {
                const name = path.basename(stat.path, ".js");
                const TaskClass = require(stat.path);
                const task = new TaskClass(this);
                taskClasses[task.name()] = task;
            });
        }

        return taskClasses;
    }
    buildTasks(taskClasses) {
        const tasks = {};

        objectForEach(taskClasses, (task, name) => {
            tasks[name] = task.build();
        });

        return tasks;
    }
    getTasks() {
        const defaults = this.requreTasks(
                path.join(this.appConfigPath, "tasks")
            ),
            overrides = this.requreTasks(path.join(__dirname, "tasks")),
            taskClasses = extend(defaults, overrides);

        this.taskClasses = taskClasses;
        this.tasks = this.buildTasks(taskClasses);

        return this.tasks;
    }

    getLocalAddress() {
        let interfaces = os.networkInterfaces();

        for (let key of interfaces) {
            let inter = interfaces[key];

            for (let k of inter) {
                let address = inter[k];

                if (address.family === "IPv4" && !address.internal) {
                    return address.address;
                }
            }
        }

        return "localhost";
    }
}

module.exports = App;
