let EventEmitter = require("@nathanfaucett/event_emitter");

class Task extends EventEmitter {
    constructor(app) {
        super(-1);
        this.app = app;
    }
    name() {
        return "unknown-name";
    }
    description() {
        return "";
    }
    build() {
        const _this = this;

        function task(callback) {
            return _this.run(callback);
        }

        task.description = this.description();

        this.emit("build");

        return task;
    }
    run() {}
}

module.exports = Task;
