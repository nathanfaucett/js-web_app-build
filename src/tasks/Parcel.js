const Bundler = require("parcel-bundler"),
    Task = require("../Task");

class Parcel extends Task {
    constructor(app, options) {
        super(app);

        this.options = options || {};
        this.bundlers = [];
    }
    name() {
        return "parcel";
    }
    description() {
        return "compiles assets with parcel";
    }
    run(callback) {
        const bundler = new Bundler(
            this.app.paths.htmlIndexSrc,
            this.getOptions()
        );

        this.bundlers.push(bundler);
        bundler
            .bundle()
            .then(bundle => callback())
            .catch(callback);
    }
    close() {
        this.bundlers.forEach(bundler => bundler.stop());
        this.bundlers.length = 0;
    }
    getOptions() {
        const paths = this.app.paths;
        const env = this.app.getEnv();

        return {
            outDir: paths.appDest,
            outFile: paths.htmlIndexDest,
            publicUrl: "./",
            watch: env !== "production",
            cache: env !== "production",
            cacheDir: paths.cacheDir,
            minify: env === "production",
            target: "browser",
            https: false,
            logLevel: 3,
            hmrPort: 0,
            sourceMaps: true,
            hmrHostname: "",
            detailedReport: false
        };
    }
}

module.exports = Parcel;
