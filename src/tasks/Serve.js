const connect = require("gulp-connect"),
    Task = require("../Task");

class Serve extends Task {
    constructor(app) {
        super(app);

        this.servers = [];
    }
    name() {
        return "serve";
    }
    description() {
        return "serve app using gulp connect";
    }
    run(callback) {
        const server = connect.server(
            {
                root: this.app.paths.appDest,
                port: this.app.getEnvVar("PORT", 8080),
                livereload: false
            },
            callback
        );

        this.servers.push(server);
    }
    close() {
        this.servers.forEach(server => server.close());
        this.servers.length = 0;
    }
}

module.exports = Serve;
