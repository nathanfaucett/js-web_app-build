const series = require("@nathanfaucett/series"),
    Task = require("../Task");

class Watch extends Task {
    constructor(app) {
        super(app);

        const reload = callback => {
            this.app.getTaskClass("livereload").reload();
            callback();
        };

        this.watchers = [];

        this.watch = (glob, options, callback) => {
            const watcher = this.app.watch(glob, options, callback);
            this.watchers.push(watcher);
            return watcher;
        };

        this.config = () =>
            series([this.app.getTask("config"), reload], () => {});

        this.locales = () =>
            series([this.app.getTask("locales"), reload], () => {});
    }
    name() {
        return "watch";
    }
    description() {
        return "watches app files";
    }
    close() {
        this.watchers.forEach(watcher => {
            watcher.close();
        });
        this.watchers.length = 0;
    }
    run(callback) {
        const paths = this.app.paths;

        this.watch([paths.configSrc + "/config/**/*.js"], this.config);
        this.watch([paths.localeSrc + "/**/*.json"], this.locales);

        callback();
    }
}

module.exports = Watch;
