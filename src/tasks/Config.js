const fileUtils = require("@nathanfaucett/file_utils"),
    Task = require("../Task");

class Config extends Task {
    constructor(app) {
        super(app);

        this.configDest = this.app.paths.jsSrc + "/config.js";
    }
    name() {
        return "config";
    }
    description() {
        return "create app config file";
    }
    run(callback) {
        fileUtils.writeFile(
            this.configDest,
            "module.exports = " +
                JSON.stringify(this.app.getConfig(), null, 4) +
                ";\n",
            callback
        );
    }
}

module.exports = Config;
