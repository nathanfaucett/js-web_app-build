const vfs = require("vinyl-fs"),
    minifyJSON = require("gulp-jsonminify"),
    Task = require("../Task");

class MinifyLocales extends Task {
    name() {
        return "minify-locales";
    }
    description() {
        return "minify locales";
    }
    run() {
        return vfs
            .src(this.app.paths.localeDest + "/**/*.json")
            .pipe(minifyJSON())
            .pipe(vfs.dest(this.app.paths.localeDest));
    }
}

module.exports = MinifyLocales;
