const fs = require("fs"),
    path = require("@nathanfaucett/file_path"),
    once = require("@nathanfaucett/once"),
    extend = require("@nathanfaucett/extend"),
    flattenObject = require("@nathanfaucett/flatten_object"),
    objectForEach = require("@nathanfaucett/object-for_each"),
    filePath = require("@nathanfaucett/file_path"),
    fileUtils = require("@nathanfaucett/file_utils"),
    Task = require("../Task");

class Locales extends Task {
    name() {
        return "locales";
    }
    description() {
        return "flatten locales files from config/locale/{locale}";
    }
    run(callback) {
        const locales = this.app.config.locales || ["en"],
            flatLocaleMode = this.app.config.flatLocaleMode,
            onceCallback = once(callback);

        let length = locales.length;

        const onDone = err => {
            if (err) {
                onceCallback(err);
            } else if (--length === 0) {
                onceCallback();
            }
        };

        objectForEach(locales, locale => {
            const localeObject = {};

            fileUtils.dive(
                path.join(this.app.paths.localeSrc, locale),
                (stat, next) => {
                    fs.readFile(stat.path, (err, buffer) => {
                        let json;

                        if (err) {
                            next(err);
                        } else {
                            try {
                                json = JSON.parse(buffer.toString());
                            } catch (e) {
                                console.error("Error parsing " + stat.path, e);
                                next(e);
                                return;
                            }

                            if (flatLocaleMode) {
                                json = flattenObject(json);
                            }

                            extend(localeObject, json);

                            next();
                        }
                    });
                },
                error => {
                    if (error) {
                        onDone(error);
                    } else {
                        fileUtils.writeFile(
                            filePath.join(
                                this.app.paths.localeDest,
                                locale + ".json"
                            ),
                            JSON.stringify(localeObject, null, 2),
                            onDone
                        );
                    }
                }
            );
        });
    }
}

module.exports = Locales;
