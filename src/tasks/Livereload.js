const livereload = require("@nathanfaucett/livereload"),
    debounce = require("@nathanfaucett/debounce"),
    Task = require("../Task");

class Livereload extends Task {
    constructor(app) {
        super(app);

        this.reload = debounce(() => {
            livereload.reload();
        }, 100);
    }
    name() {
        return "livereload";
    }
    description() {
        return "livereload";
    }
    run(callback) {
        livereload.listen({
            port: this.app.config.liveReloadPort
        });
        callback();
    }
    close() {
        livereload.close();
    }
}

module.exports = Livereload;
