const series = require("@nathanfaucett/series"),
    parallel = require("@nathanfaucett/parallel"),
    Task = require("../Task");

class Build extends Task {
    name() {
        return "build";
    }
    description() {
        return "builds app in NODE_ENV";
    }
    run(callback) {
        const tasks = [
            this.app.getTask("clean"),
            this.app.getTask("config"),
            parallel.from([
                this.app.getTask("parcel"),
                this.app.getTask("locales")
            ])
        ];

        if (this.app.getEnv() === "production") {
            tasks.push(this.app.getTask("minify-locales"));
        }

        series(tasks, callback);
    }
}

module.exports = Build;
