const del = require("del"),
    Task = require("../Task");

class Clean extends Task {
    name() {
        return "clean";
    }
    description() {
        return "clean paths for current NODE_ENV";
    }
    run() {
        return del([
            this.app.getTaskClass("config").configDest,
            this.app.paths.appDest
        ]);
    }
}

module.exports = Clean;
