const series = require("@nathanfaucett/series"),
    Task = require("../Task");

class Default extends Task {
    name() {
        return "default";
    }
    description() {
        return "runs build tasks, if not production watches files and serves app";
    }
    run(callback) {
        const tasks = [];

        if (this.app.getEnv() !== "production") {
            tasks.push(this.app.getTask("livereload"));
            tasks.push(this.app.getTask("serve"));
            tasks.push(this.app.getTask("watch"));
        }

        tasks.push(this.app.getTask("build"));

        series(tasks, callback);
    }
}

module.exports = Default;
