js-katan-build
=======

easier build tool for the front end

# Example

app structure

```
app/
  js/
  css/
  img/
  index.html
config/
  locales/
    en/
    ...
  app.js
Gulpfile.js
package.json
```

config/app.js
```javascript
const path = require("path"),
    App = require("katan-build").App;

module.exports = new App(path.dirname(__dirname));
```

Gulpfile.js
```javascript
const gulp = require("gulp"),
    app = require("./config/app");

app.useGulp(gulp);
```
