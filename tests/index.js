const tape = require("tape"),
    fs = require("fs"),
    path = require("path"),
    fileUtils = require("@nathanfaucett/file_utils"),
    katan = require("..");

const loadApp = () => {
    const fullPath = require.resolve("./config/app");
    delete require.cache[fullPath];
    return require(fullPath);
};

const cleanCache = () => {
    const cachePath = path.join(__dirname, ".cache");

    if (fs.existsSync(cachePath)) {
        fileUtils.removeSync(cachePath);
    }
};

const runInNodeEnv = (env, callback) => {
    const NODE_ENV = process.env.NODE_ENV;

    process.env.NODE_ENV = env;

    callback(() => {
        process.env.NODE_ENV = NODE_ENV;
    });
};

tape("App run build", assert => {
    runInNodeEnv("development", callback => {
        const app = loadApp();

        cleanCache();

        app.runTask("default", () => {
            app.getTaskClass("parcel").close();
            app.getTaskClass("watch").close();
            app.getTaskClass("serve").close();
            app.getTaskClass("livereload").close();

            callback();
            cleanCache();

            assert.end();
        });
    });
});

tape("App run production build", assert => {
    runInNodeEnv("production", callback => {
        const app = loadApp();

        app.runTask("default", () => {
            app.getTaskClass("parcel").close();
            app.getTaskClass("watch").close();
            app.getTaskClass("serve").close();
            app.getTaskClass("livereload").close();

            callback();

            assert.end();
            process.exit();
        });
    });
});
