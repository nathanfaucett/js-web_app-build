const path = require("path"),
    katan = require("../..");

module.exports = new katan.App(path.dirname(__dirname));
